﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace soa
{
    //todo2: наследование вообще не оправдано, можно было просто выделит ьинтерфейс ISolver { Output Solve (Input input) }
    //todo2: один класс - одна отвественность. в данном случае - рассчет, а ввод и вывод это уже другое дело совершенно
    abstract class Lab : ILab
    {
        protected Input input;
        protected Output output;
        protected abstract Input ReadInput(bool isJson = true);
        public abstract void WriteAnswer(bool isJson=true);
        private Output CalculateOutput(Input input, bool isJson = true)
        {
            Output output = new Output();
            output.SumResult = input.Sums.Sum() * (decimal)input.K;
            output.MulResult = input.Muls.Aggregate(1, (x, y) => x * y);
            output.SortedInputs = input.Muls.Select(x => (decimal)x).Concat(input.Sums)
                .Select(x => (isJson) ? x + 1.0m - 1.0m : x).OrderBy(x => x).ToArray();
            return output;
        }
        public void GetData(bool isJson = true)
        {
            input = ReadInput(isJson);
            output = CalculateOutput(input, isJson);
        }
        public abstract void Solve();
    }
}
