﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace soa
{
    sealed class Lab3 : Lab
    {
        HttpListener Listener { get; set; }
        private HttpListenerRequest request;
        private HttpListenerResponse response;

        //todo3: три нижних метода - это инфраструктура, они не должны лежат ьрядом с бизнесом
        protected override Input ReadInput(bool isJson = true)
        {
            Input input = new Input();
            var reader = new StreamReader(request.InputStream);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            input = (Input)serializer.Deserialize(reader.ReadToEnd(), typeof(Input));
            reader.Close();
            return input;
        }

        public override void WriteAnswer(bool isJson = true)
        {
            var writer = new StreamWriter(response.OutputStream);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            writer.Write(serializer.Serialize(output));
            writer.Close();
        }

        public bool HandleContext(HttpListenerContext context)
        {
            request = context.Request;
            response = context.Response;
            string method = request.Url.AbsolutePath;
            try {
                return (bool)GetType().GetMethod(method.Remove(0, 1)).Invoke(this, null);
            }
            catch
            {
               response.Abort();
               return true;
            }
        }

        public override void Solve()
        {
            string port = Console.ReadLine();
            Listener = new HttpListener();
            Listener.Prefixes.Add("http://127.0.0.1:" + port + "/");
            Listener.Start();
            while (HandleContext(Listener.GetContextAsync().Result)) { }
            Listener.Stop();
        }

        public bool Ping()
        {
            response.Abort();
            return true;
        }


        //todo3: три нижних метода - это бизнес, они не должны лежат ьрядом с инфратсруктурой
        public bool PostInputData()
        {
            GetData();
            response.Abort();
            return true;
        }

        public bool GetAnswer()
        {
            WriteAnswer();
            response.Close();
            return true;
        }

        public bool Stop()
        {
            response.Abort();
            return false;
        }

    }
}
