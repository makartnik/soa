﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace soa
{
    interface ILab
    {
        void GetData(bool isJson);
        void WriteAnswer(bool isJson);
        void Solve();
    }
}
