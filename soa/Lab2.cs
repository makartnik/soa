﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Net.Http;

namespace soa
{
    sealed class Lab2 : Lab
    {
        Uri BaseUri { get; set; }
        protected override Input ReadInput(bool isJson = true)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = BaseUri;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string inputData = client.GetStringAsync("GetInputData").Result;
            return (Input)serializer.Deserialize(inputData, typeof(Input));
        }

        public override void WriteAnswer(bool isJson = true)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = BaseUri;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            client.PostAsync("WriteAnswer", new StringContent(serializer.Serialize(output))).Wait();
        }

        public HttpStatusCode PingServer()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = BaseUri;
            HttpResponseMessage message = client.GetAsync("Ping").Result;
            return message.StatusCode;
        }

        public override void Solve()
        {
            string port = Console.ReadLine();
            BaseUri = new Uri("http://127.0.0.1:" + port);
            while (!PingServer().Equals(HttpStatusCode.OK)) { }
            GetData();
            WriteAnswer();
        }
    }
}
