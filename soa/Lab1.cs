﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace soa
{
    sealed class Lab1 : Lab
    {
        private string type;
        protected override Input ReadInput(bool isJson = true)
        {
            Input input = new Input();
            if (isJson)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                input = (Input)serializer.Deserialize(Console.ReadLine(), typeof(Input));
            }
            else
            {
                XmlReader reader = XmlReader.Create(Console.In);
                XmlSerializer serializer = new XmlSerializer(typeof(Input));
                input = (Input)serializer.Deserialize(reader);
            }
            return input;
        }

        public override void WriteAnswer(bool isJson = true)
        {
            if (isJson)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Console.WriteLine(serializer.Serialize(output));
            }
            else
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Output));
                XmlWriter writer = XmlWriter.Create(Console.Out, new XmlWriterSettings { OmitXmlDeclaration = true });
                XmlSerializerNamespaces names = new XmlSerializerNamespaces();
                names.Add("", "");
                serializer.Serialize(writer, output, names);
            }
        }

        public override void Solve()
        {
            type = Console.ReadLine();
            GetData(type.Equals("Json"));
            WriteAnswer(type.Equals("Json"));
        }
    }
}
